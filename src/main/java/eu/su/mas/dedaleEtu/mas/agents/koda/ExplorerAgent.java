package eu.su.mas.dedaleEtu.mas.agents.koda;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.koda.ExploreBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;

import jade.core.AID;
import jade.core.behaviours.Behaviour;


public class ExplorerAgent extends AbstractDedaleAgent {
    private static final long serialVersionUID = 1243245;
    private MapRepresentation myMap; // Map representation of the environment
    private List<String> explorerAgentList; // List of other explorer agents
    private List<String> collectorAgentList; // List of collector agents

    private AID coordinatorAgent;

    @Override
    protected void setup() {
        super.setup();
        explorerAgentList = new ArrayList<>();
        collectorAgentList = new ArrayList<>();
        final Object[] args = getArguments();

        if (args.length < 2) {
            System.err.println("Error while creating the agent, names of agent to contact expected");
            System.exit(-1);
        } else {
            // Assuming collector agent names are passed after explorer agent names
            int i = 2; // Starting index for explorer agents
            while (i < args.length && !(args[i] instanceof CollectorAgent)) {
                explorerAgentList.add((String) args[i]);
                i++;
            }
        }
        collectorAgentList.add("Collect1");
        collectorAgentList.add("Collect2");
        collectorAgentList.add("Collect3");
        collectorAgentList.add("Collect4");
        coordinatorAgent=new AID("TheBrain",AID.ISLOCALNAME);

        List<Behaviour> lb = new ArrayList<Behaviour>();
        lb.add(new ExploreBehaviour(this, myMap, explorerAgentList, collectorAgentList, coordinatorAgent)); // Exploration behavior to share map with other explorers
        // Add behaviours to the agent
        addBehaviour(new startMyBehaviours(this, lb));
        System.out.println("the agent " + this.getLocalName() + " is started");
    }

    protected void takeDown() {
        super.takeDown();
    }
}

