package eu.su.mas.dedaleEtu.mas.behaviours.koda;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.gsLocation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agents.observerAgent.TreasureK;
import eu.su.mas.dedaleEtu.mas.agents.koda.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class ExploreBehaviour extends Behaviour {

    private boolean finished = false;

    private MapRepresentation myMap;
    private List<String> explorerAgentList;
    private List<String> collectorAgentList; // List of collector agents
    private boolean mapShared;
    private int Deadlockcounter;
    private String preposition=null;
    private int Counter=0;
    private List<String> helpCalls = null;
    private AID coordinatorAgent;
    private String randomOpenNode ="";

    public ExploreBehaviour(final AbstractDedaleAgent myAgent, MapRepresentation myMap, List<String> agentList, List<String> collectoragentList,AID coordinatorAgent) {
        super(myAgent);
        this.myMap = myMap;
        this.explorerAgentList = agentList;
        this.collectorAgentList =collectoragentList;
        this.mapShared = false;
        this.coordinatorAgent=coordinatorAgent;

    }

    @Override
    public void action() {

        if (this.myMap == null)
        {
            this.myMap = new MapRepresentation();
        }


        Location myPosition = ((AbstractDedaleAgent) this.myAgent).getCurrentPosition();
        if(myPosition.getLocationId().equals(randomOpenNode))
        {
            randomOpenNode="";
            Deadlockcounter=0;
        }

        respondToPositionRequest();
        helpCalls=handleHelpRequestandMoveToCollectorPosition(myPosition.getLocationId());

        Counter++;
        if (myPosition != null) {
            List<Couple<Location, List<Couple<Observation, Integer>>>> lobs = ((AbstractDedaleAgent) this.myAgent).observe();

            if(myPosition.getLocationId().equals(preposition)) {
                Deadlockcounter++;
            } else {
                preposition = myPosition.getLocationId();
            }

            try {
                this.myAgent.doWait(500);
            } catch (Exception e) {
                e.printStackTrace();
            }

            this.myMap.addNode(myPosition.getLocationId(), MapRepresentation.MapAttribute.closed);


            //Send Treasure Location
            for (Couple<Location, List<Couple<Observation, Integer>>> couple : lobs) {
                Location location = couple.getLeft();
                List<Couple<Observation, Integer>> observations = couple.getRight();

                for (Couple<Observation, Integer> observation : observations) {
                    // Check if the node contains gold or diamond treasure
                    String treasureType = String.valueOf(observation.getLeft());
                    if(treasureType.equals("Gold")||(treasureType.equals("Diamond")))
                    {
                        if (treasureType.equals("Gold")) {
                            sendtreasurelocation(0, location);
                        } else {
                            sendtreasurelocation(1, location);
                        }
                    }
                }
            }


            // Movement
            String nextNodeId = null;
            Iterator<Couple<Location, List<Couple<Observation, Integer>>>> iter = lobs.iterator();
            List<String> neighborNodeIds = new ArrayList<>();

            if(helpCalls.isEmpty())
            {
                while (iter.hasNext()) {
                    Location accessibleNode = iter.next().getLeft();
                    boolean isNewNode = this.myMap.addNewNode(accessibleNode.getLocationId());

                    if (!myPosition.getLocationId().equals(accessibleNode.getLocationId())) {
                        neighborNodeIds.add(accessibleNode.getLocationId());
                        if (isNewNode) {
                            this.myMap.addEdge(myPosition.getLocationId(), accessibleNode.getLocationId());

                            // Only set nextNodeId if it is not already assigned and the node is new
                            if (nextNodeId == null) {
                                nextNodeId = accessibleNode.getLocationId();
                            }
                        }
                    }
                }
            }
            else
            {
                nextNodeId=helpCalls.get(0);
            }



            if (!this.myMap.hasOpenNode()) {
                finished = true;
                System.out.println(this.myAgent.getLocalName() + " - Exploration successfully done, behaviour removed.");
            } else {
                if (nextNodeId == null) {
                    nextNodeId = this.myMap.getShortestPathToClosestOpenNode(myPosition.getLocationId()).get(0);
                }
                if(Deadlockcounter==2)
                {
                    randomOpenNode = handleDeadlock(myPosition.getLocationId(), neighborNodeIds);
                    Deadlockcounter=0;
                }
                if(!(randomOpenNode.isEmpty()))
                {
                    nextNodeId = myMap.getShortestPath(myPosition.getLocationId(),randomOpenNode).get(0);
                }
                if(Counter==15)
                {
                    if (!mapShared) {
                        sendMapInformation();
                        mapShared = true;
                    } else {
                        // Receive the map from other explorers and update the agent's map
                        handleMapInformation();
                        mapShared = false;
                    }
                    Counter=0;
                }
                if (!nextNodeId.equals(preposition)) {
                    myMap.addNode(myPosition.getLocationId(), MapRepresentation.MapAttribute.closed);
                    ((AbstractDedaleAgent) this.myAgent).moveTo(new gsLocation(nextNodeId));
                }
            }
        }
    }
    private void sendMapInformation() {

        //sendMap(explorerAgentList);
        sendMapToAgent(coordinatorAgent);
    }

    private void handleMapInformation() {

        AID [] Test = new AID(myAgent.getLocalName(),AID.ISLOCALNAME).getResolversArray();
        MessageTemplate mt=MessageTemplate.and(
                MessageTemplate.MatchProtocol("SHARE-TOPO"),
                MessageTemplate.MatchReceiver(Test));
        ACLMessage receivedMessage = myAgent.receive(mt);

        if (receivedMessage != null) {
            try {
                SerializableSimpleGraph<String, MapRepresentation.MapAttribute> receivedGraph = (SerializableSimpleGraph<String, MapRepresentation.MapAttribute>) receivedMessage.getContentObject();
                this.myMap.mergeMap(receivedGraph); // Merging received map with current map
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Empty Map sent:" + myAgent.getLocalName());
            //block();
        }
    }

    private void sendtreasurelocation(int treasuretype, Location location)
    {

        String Treasure = "";
        if(treasuretype==0)
        {
            Treasure="gold";
        }
        else
        {
            Treasure="diamond";
        }
        // Send a message
        ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
        message.setContent(Treasure+";" + location.toString());
        for (String s : collectorAgentList) {
            AID agentAID;
            agentAID = new AID(s+"@Ithaq");
            message.addReceiver(agentAID);
        }
        System.out.println("Send message:"+message);
        myAgent.send(message);
    }
    private void sendMapToAgent(AID receiver) {
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setProtocol("SHARE-TOPO");
        msg.setSender(this.myAgent.getAID());
        msg.addReceiver(receiver);

        SerializableSimpleGraph<String, MapRepresentation.MapAttribute> sg = this.myMap.getSerializableGraph();
        try {
            msg.setContentObject(sg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Sent map to Coordinator");
        ((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
    }
    private List<String> handleHelpRequestandMoveToCollectorPosition(String currentPosition) {
        List<String> helpCalls = new ArrayList<>();
        ACLMessage helpRequest = myAgent.receive(MessageTemplate.MatchProtocol("HELP-REQUEST"));
        if (helpRequest != null) {
            String collectorPosition = helpRequest.getContent();
            // Extracting the position from the help call
            helpCalls = this.myMap.getShortestPath(currentPosition, collectorPosition);

        }

        return helpCalls;
    }

    private void respondToPositionRequest() {
        MessageTemplate mt = MessageTemplate.MatchProtocol("REQUEST-POSITION");
        ACLMessage msg = myAgent.receive(mt);
        if (msg != null) {
            ACLMessage reply = msg.createReply();
            reply.setPerformative(ACLMessage.INFORM);
            reply.setProtocol("RESPONSE-POSITION");
            Location myPosition = ((AbstractDedaleAgent) this.myAgent).getCurrentPosition();
            reply.setContent(myAgent.getLocalName()+";"+myPosition.getLocationId());
            myAgent.send(reply);
        }
    }

    private String handleDeadlock(String currentPosition, List<String> neighborNodeIds) {
        // Remove the previous position from the list of neighbors to avoid going back
        neighborNodeIds.remove(preposition);

        if (neighborNodeIds.isEmpty()) {
            // No available neighbors, backtrack to previous position
            return preposition;
        } else {
            // Calculate a new path that avoids the blocked node
            return findAlternativePath(currentPosition);
        }
    }

    private String findAlternativePath(String currentPosition) {
        Random random = new Random();

        List<String> openNodes = myMap.getOpenNodes();
        if (openNodes.isEmpty()) {
            // No open nodes available, handle accordingly
            return currentPosition;  // Fallback to current position
        } else {
            String candidateNode;
            for (int attempts = 0; attempts < openNodes.size(); attempts++) {
                candidateNode = openNodes.get(random.nextInt(openNodes.size()));
                if (myMap.getShortestPath(candidateNode, currentPosition) != null) {
                    return candidateNode;  // Found a reachable open node
                }
            }
            // No reachable open nodes found after trying all options
            return currentPosition;  // Fallback to current position
        }
    }


    @Override
    public boolean done() {
        return finished;
    }
}