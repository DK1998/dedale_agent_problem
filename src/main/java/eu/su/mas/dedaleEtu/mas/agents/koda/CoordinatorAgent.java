package eu.su.mas.dedaleEtu.mas.agents.koda;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.koda.CoordinatorBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.koda.ExploreBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.List;


public class CoordinatorAgent extends AbstractDedaleAgent {
    private static final long serialVersionUID = 2134324235;
    private List<String> explorerAgentList; // List of other explorer agents
    private List<AID> collectorAgentList; // List of collector agents

    private List<AID> agentList; // List of collector agents

    private MapRepresentation myMap;

    protected void setup() {
        super.setup();
        explorerAgentList = new ArrayList<>();
        collectorAgentList = new ArrayList<>();
        agentList = new ArrayList<>();

        explorerAgentList.add("Explo1");
        explorerAgentList.add("Explo2");
        explorerAgentList.add("Explo3");
        explorerAgentList.add("Explo4");
        collectorAgentList.add(new AID("Collect1",AID.ISLOCALNAME));
        collectorAgentList.add(new AID("Collect2",AID.ISLOCALNAME));
        collectorAgentList.add(new AID("Collect3",AID.ISLOCALNAME));
        collectorAgentList.add(new AID("Collect4",AID.ISLOCALNAME));
        agentList.add(new AID("Collect1",AID.ISLOCALNAME));
        agentList.add(new AID("Collect2",AID.ISLOCALNAME));
        agentList.add(new AID("Collect3",AID.ISLOCALNAME));
        agentList.add(new AID("Collect4",AID.ISLOCALNAME));
        agentList.add(new AID("Explo1",AID.ISLOCALNAME));
        agentList.add(new AID("Explo2",AID.ISLOCALNAME));
        agentList.add(new AID("Explo3",AID.ISLOCALNAME));
        agentList.add(new AID("Explo4",AID.ISLOCALNAME));

        List<Behaviour> lb = new ArrayList<Behaviour>();
        lb.add(new CoordinatorBehaviour(this, agentList, explorerAgentList, collectorAgentList, myMap)); // Exploration behavior to share map with other explorers
        addBehaviour(new startMyBehaviours(this, lb));
    }

}