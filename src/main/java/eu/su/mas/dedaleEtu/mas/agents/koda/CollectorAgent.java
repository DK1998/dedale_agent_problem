package eu.su.mas.dedaleEtu.mas.agents.koda;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.koda.CollectorBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.Behaviour;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CollectorAgent extends AbstractDedaleAgent {
    private static final long serialVersionUID = -1784844593772934434L;
    private MapRepresentation map; // Map representation of the environment
    private List<AID> collectorAgentList; // List of collector agents

    private AID coordinatorAgent;

    private Set<Couple<Observation, Integer>> ExpertiseLevel;

    @Override
    protected void setup() {
        super.setup();
        collectorAgentList = new ArrayList<>();
        //get the parameters added to the agent at creation (if any)
        final Object[] args = getArguments();


        if(args.length==0){
            System.err.println("Error while creating the agent, names of agent to contact expected");
            System.exit(-1);
        }else{
            int i=2;
            while (i<args.length) {
                AID agentAID = new AID((String) args[i]);
                collectorAgentList.add(agentAID);
                i++;
            }
        }
        String Treasure = String.valueOf(this.getMyTreasureType());
        int Treasuretype =0;
        if(Treasure.contains("Gold"))
        {
            Treasuretype = 0;
        }
        else
        {
            Treasuretype = 1;
        }
        coordinatorAgent = new AID("TheBrain",AID.ISLOCALNAME);
        ExpertiseLevel = this.getMyExpertise();
        Object[]ArrayExpertise = ExpertiseLevel.toArray();
        List<Behaviour> lb = new ArrayList<Behaviour>();
        lb.add(new CollectorBehaviour(this, map, this.getBackPackFreeSpace(), Treasuretype, ArrayExpertise, coordinatorAgent));
        addBehaviour(new startMyBehaviours(this,lb));
        System.out.println("the  agent "+this.getLocalName()+ " is started");

    }
}
