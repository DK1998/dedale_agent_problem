package eu.su.mas.dedaleEtu.mas.behaviours.koda;

import com.sun.xml.bind.v2.schemagen.xmlschema.Any;
import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.gsLocation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.UnreachableException;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import org.graphstream.ui.javafx.util.AttributeUtils;

import java.util.*;

public class CollectorBehaviour extends CyclicBehaviour {
    private Object[] ExpertiseLevels;
    private MapRepresentation map;
    private int backpackCapacity;
    private boolean OnTreasureHunt;
    private int collectedGold;
    private int collectedDiamonds;

    private int AgentTreasureType;
    private AID coordinatorAgent;

    private List<Couple> treasureLocations; // Map to store treasure locations with type
    private List<Couple> validTreasureLocations;

    private int Treasuretype;
    private String closestTreasureLocation;

    private List<Couple> ArrayExpertise;

    public CollectorBehaviour(Agent agent, MapRepresentation map, List<Couple<Observation, Integer>> backpackCapacity, int AgentTreasureType, Object[] ExpertiseLevel, AID coordinatorAgent) {
        super(agent);
        this.map = map;
        this.ExpertiseLevels = ExpertiseLevel;
        Couple Test = backpackCapacity.get(AgentTreasureType);
        this.backpackCapacity = (int) Test.getRight();
        this.OnTreasureHunt = false;
        this.collectedGold = 0;
        this.collectedDiamonds = 0;
        this.AgentTreasureType = AgentTreasureType;
        this.coordinatorAgent = coordinatorAgent;

    }



    @Override
    public void action() {

        if (this.map == null)
        {
            this.map = new MapRepresentation();
            map.addNode(((AbstractDedaleAgent) this.myAgent).getCurrentPosition().getLocationId(), MapRepresentation.MapAttribute.closed);
        }
        Location currentLocation = ((AbstractDedaleAgent) this.myAgent).getCurrentPosition();
        try {
            this.myAgent.doWait(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        treasureLocations = new ArrayList<>();
        validTreasureLocations = new ArrayList<>();

        //Test if Treasure Hunt is possible
        updateTreasureLocation();
        if(OnTreasureHunt==false)
        {
            OnTreasureHunt=treasureHuntStart(currentLocation.getLocationId());
        }



        //Update Map
        MessageTemplate mt=MessageTemplate.and(
                MessageTemplate.MatchProtocol("SHARE-TOPO"),
                MessageTemplate.MatchPerformative(ACLMessage.INFORM));
        ACLMessage receivedMessage = myAgent.receive(mt);
        if(receivedMessage!=null)
        {
            handleMapInformation(receivedMessage);
            System.out.println("Map received");
        }

        // Check if at TreasureLocation
        if(OnTreasureHunt)
        {
            if (Objects.equals(closestTreasureLocation, currentLocation.getLocationId())) {
                List<Couple<Location, List<Couple<Observation, Integer>>>> lobs = ((AbstractDedaleAgent) this.myAgent).observe();

                int locktile = lobs.get(0).getRight().get(3).getRight();
                int strengthtile = lobs.get(0).getRight().get(3).getRight();

                Couple Lockpickingskills = (Couple) ExpertiseLevels[1];
                Couple Strengthskills = (Couple) ExpertiseLevels[0];

                int pickedvalue = ((AbstractDedaleAgent) this.myAgent).pick();

                if (pickedvalue == 0) {
                    if ((locktile != (int) Lockpickingskills.getRight()) && (strengthtile != (int) Strengthskills.getRight())) {
                        sendHelpRequest(currentLocation.getLocationId());
                    }
                } else {
                    ((AbstractDedaleAgent) this.myAgent).openLock(Observation.ANY_TREASURE);
                    if (backpackCapacity > 0) {
                        // Update the backpack capacity and collected treasures

                        if ((Treasuretype == 0) && (AgentTreasureType == 0)) {


                            collectedGold = collectedGold + pickedvalue;
                            ((AbstractDedaleAgent) this.myAgent).pick();
                            backpackCapacity = backpackCapacity - pickedvalue;

                            System.out.println("Collected gold: " + collectedGold);
                        } else if ((Treasuretype == 1) && (AgentTreasureType == 1)) {
                            collectedDiamonds = collectedDiamonds + pickedvalue;
                            System.out.println("Collected diamonds: " + collectedDiamonds);
                            backpackCapacity = backpackCapacity - pickedvalue;
                        }

                        // Mark the location as visited and remove the treasure from the map
                        map.addNode(currentLocation.getLocationId(), MapRepresentation.MapAttribute.closed);
                        OnTreasureHunt = false;
                        //sendUpdateToCoordinator(treasureType, currentLocation.getLocationId()); // Inform the coordinator about the collected treasure
                    }
                }

            }
            else {
                List<String> Nodes = treasureHunt(currentLocation.getLocationId());
                if(!(Nodes==null))
                {
                    System.out.println("Nodes:"+Nodes);
                    map.addNode(currentLocation.getLocationId(), MapRepresentation.MapAttribute.closed);
                    ((AbstractDedaleAgent) this.myAgent).moveTo(new gsLocation(Nodes.get(0)));
                }
            }
        }
    }

    public boolean treasureHuntStart(String currentposition)
    {
        List<String> TreasurePath = new ArrayList<>();
        if(treasureLocations != null) {
            for (Couple Item : treasureLocations) {
                int i = (int) Item.getLeft();
                TreasurePath = map.getShortestPath(String.valueOf(Item.getRight()), currentposition);
                if ((TreasurePath != null) && (i == Treasuretype)) {
                    validTreasureLocations.add(Item);
                    System.out.println(this.myAgent.getName()+": Let the Hunt begin!");
                    return true;
                }
            }
        }
        return false;
    }

    private void updateTreasureLocation() {
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        ACLMessage receivedMessage = myAgent.receive(mt);

        if (receivedMessage != null) {
            String[] contentParts = receivedMessage.getContent().split(";");
            int treasureType = contentParts[0].equals("gold") ? 0 : 1;
            String treasurePosition = contentParts[1];

            Couple<Integer, String> newTreasureLocation = new Couple<>(treasureType, treasurePosition);

            // Check if the treasure location already exists in the list
            if (!treasureLocationExists(newTreasureLocation)) {
                treasureLocations.add(newTreasureLocation);
            }
        }
    }

    private boolean treasureLocationExists(Couple<Integer, String> newTreasureLocation) {
        for (Couple<Integer, String> existingLocation : treasureLocations) {
            if (existingLocation.getRight().equals(newTreasureLocation.getRight()) &&
                    existingLocation.getLeft().equals(newTreasureLocation.getLeft())) {
                return true; // The location already exists in the list
            }
        }
        return false; // The location is not in the list
    }
    public List<String> treasureHunt(String currentposition) {
        updateTreasureLocation(); // Update treasure locations
        closestTreasureLocation = null;
        int minDistance = Integer.MAX_VALUE;

        for (Couple item : validTreasureLocations) {
            int treasureType = (int) item.getLeft();
            String treasurePosition = (String) item.getRight();
            List<String> path = map.getShortestPath(currentposition, treasurePosition);

            if (path != null && !path.isEmpty() && treasureType == Treasuretype) {
                int distance = path.size();
                if (distance < minDistance) {
                    minDistance = distance;
                    closestTreasureLocation = treasurePosition;
                }
            }
        }

        if (closestTreasureLocation != null) {
            return map.getShortestPath(currentposition, closestTreasureLocation);
        } else {
            // Fallback strategy if no path is found
            return null;
        }
    }


    private void handleMapInformation(ACLMessage receivedMessage) {
        try {
            SerializableSimpleGraph<String, MapRepresentation.MapAttribute> receivedGraph = (SerializableSimpleGraph<String, MapRepresentation.MapAttribute>) receivedMessage.getContentObject();
            this.map.mergeMap(receivedGraph); // Merging received map with current map
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendHelpRequest( String currentposition) {
        ACLMessage helpMsg = new ACLMessage(ACLMessage.REQUEST);
        helpMsg.setProtocol("REQUEST-HELP");
        helpMsg.addReceiver(coordinatorAgent);
        helpMsg.setContent("Need help at " + currentposition);
        myAgent.send(helpMsg);
    }


}