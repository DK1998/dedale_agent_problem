package eu.su.mas.dedaleEtu.mas.behaviours.koda;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.Location;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.List;

public class CoordinatorBehaviour extends Behaviour {
    private boolean coordinatorDone;
    private List<String> explorerPositions;

    private List<String> explorerAgentList; // List of other explorer agents
    private List<AID> collectorAgentList; // List of other explorer agents
    private List<AID> agentList; // List of other explorer agents
    private MapRepresentation myMap;

    public CoordinatorBehaviour(Agent agent, List<AID> agentList, List<String> explorerAgentList, List<AID> collectorAgentList, MapRepresentation myMap ) {
        super(agent);
        this.coordinatorDone = false;
        this.explorerAgentList = explorerAgentList;
        this.myMap = myMap;
        this.agentList =agentList;
    }

    @Override
    public void action() {

        if (this.myMap == null)
        {
            this.myMap = new MapRepresentation();
        }
        ACLMessage helpMsg = myAgent.receive(MessageTemplate.MatchProtocol("REQUEST-HELP"));
        String collectorPosition ="";
        if (helpMsg != null) {
            String[] contentParts = helpMsg.getContent().split("at");
            collectorPosition = contentParts[1];
            requestPositions();
        }
        ACLMessage positionMsg = myAgent.receive(MessageTemplate.MatchProtocol("RESPONSE-POSITION"));
        if (positionMsg != null) {
            String agentName = positionMsg.getSender().getLocalName();
            String position = positionMsg.getContent();
            explorerPositions.add(agentName + ";" + position);

            // Initialize variables to find the nearest explorer
            String nearestExplorer = null;
            int minDistance = Integer.MAX_VALUE;

            for (String explorerInfo : explorerPositions) {
                String[] parts = explorerInfo.split(";");
                String explorerName = parts[0];
                String explorerPosition = parts[1];

                // Calculate the distance to the collector's position
                int distance = myMap.getShortestPath(explorerPosition, collectorPosition).toArray().length;

                // Update the nearest explorer if this one is closer
                if (distance < minDistance) {
                    minDistance = distance;
                    nearestExplorer = explorerName;
                }
            }

            if (nearestExplorer != null) {
                // Send help request to the nearest explorer
                sendHelpRequest(nearestExplorer, collectorPosition);
            }
        }

        MessageTemplate mt = MessageTemplate.MatchProtocol("SHARE-TOPO");
        ACLMessage mapMsg = myAgent.receive(mt);

        if (mapMsg != null) {
            forwardMap(mapMsg);
        }
    }

    private void requestPositions() {
        ACLMessage positionRequestMsg = new ACLMessage(ACLMessage.REQUEST);
        positionRequestMsg.setProtocol("REQUEST-POSITION");
        for (String explorer : explorerAgentList) {
            positionRequestMsg.addReceiver(new AID(explorer, AID.ISLOCALNAME));
        }
        myAgent.send(positionRequestMsg);
    }
    private void forwardMap(ACLMessage originalMapMessage) {
        try {
            SerializableSimpleGraph<String, MapRepresentation.MapAttribute> receivedGraph = (SerializableSimpleGraph<String, MapRepresentation.MapAttribute>) originalMapMessage.getContentObject();
            this.myMap.mergeMap(receivedGraph);

            for (AID agent : agentList) {
                ACLMessage forwardMsg = new ACLMessage(ACLMessage.INFORM);
                forwardMsg.setProtocol("SHARE-TOPO");
                forwardMsg.setSender(myAgent.getAID());
                forwardMsg.addReceiver(agent);
                SerializableSimpleGraph<String, MapRepresentation.MapAttribute> sg=this.myMap.getSerializableGraph();
                forwardMsg.setContentObject(sg); // Send the merged map
                myAgent.send(forwardMsg);
            }
        } catch (IOException | UnreadableException e) {
            e.printStackTrace();
        }
    }

    public void sendHelpRequest(String nearestExplorer, String collectorPosition) {
        // Create a new ACL message
        ACLMessage helpRequest = new ACLMessage(ACLMessage.INFORM);
        helpRequest.setProtocol("HELP-REQUEST");
        helpRequest.setSender(myAgent.getAID());
        helpRequest.addReceiver(new AID(nearestExplorer, AID.ISLOCALNAME));
        helpRequest.setContent(collectorPosition);
        myAgent.send(helpRequest);
    }

    @Override
    public boolean done() {
        return coordinatorDone;
    }
}